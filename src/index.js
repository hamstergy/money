import React from 'react';
import ReactDOM from 'react-dom';
import { Route, NavLink, BrowserRouter as Router } from 'react-router-dom'
import './index.css';
import App from './App';
import Statistic from './Statistic';
import Bookmark from './Bookmark';
import Notification from './Notification';
import Important from './Important';
import Icon from './Icons';
import * as serviceWorker from './serviceWorker';



const routing = (
    <Router>
      <div>
        <footer className="App-footer">
          <NavLink exact={true} to='/' activeClassName='is-active' className="footer-item"><Icon name="people" color="#5f6067" size={35} /></NavLink>
          <NavLink to='/statistic' activeClassName='is-active' className="footer-item"><Icon name="statistic" color="#5f6067" size={35} /></NavLink>
          <NavLink to='/bookmark' activeClassName='is-active' className="footer-item"><Icon name="bookmark" color="#5f6067" size={35} /></NavLink>
          <NavLink to='/notification' activeClassName='is-active' className="footer-item"><Icon name="notification" color="#5f6067" size={35} /></NavLink>
          <NavLink to='/important' activeClassName='is-active' className="footer-item"><Icon name="important" color="#5f6067" size={35} /></NavLink>
       </footer>
        <Route exact path="/" component={App} />
        <Route path="/statistic" component={Statistic} />
        <Route path="/bookmark" component={Bookmark} />
        <Route path="/notification" component={Notification} />
        <Route path="/important" component={Important} />
      </div>
    </Router>
  )
ReactDOM.render(routing, document.getElementById('root'));
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
