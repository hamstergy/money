import React from 'react'
import './Bookmark.scss';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
class Bookmark extends React.Component {
  state = {
    food: '',
    stores: '',
    utilities: '',
    salary: ''
  };
  handleChange = (event) => {
    const input = event.target;
    const value = input.value;

    this.setState({ [input.name]: value });
  };
 
  handleFormSubmit = () => {
    const { salary,stores,utilities, food } = this.state;
    localStorage.setItem('food', food);
    localStorage.setItem('stores', stores);
    localStorage.setItem('utilities', utilities);
    localStorage.setItem('salary', salary);
  };
 
  componentDidMount() {
    const food = localStorage.getItem('food');
    const stores = localStorage.getItem('stores');
    const utilities = localStorage.getItem('utilities');
    const salary = localStorage.getItem('salary');
    this.setState({ food,stores,utilities,salary });
  }

  render() {
    return (
      <ReactCSSTransitionGroup
      transitionName="fade"
      transitionAppear={true}
      transitionAppearTimeout={1000}>
      <form className="app-form" onSubmit={this.handleFormSubmit}>
      <label>
        Salary: <input name="salary" value={this.state.salary || ''} onChange={this.handleChange}/>
        Food: <input name="food" value={this.state.food || ''} onChange={this.handleChange}/>
        Utilities: <input name="utilities" value={this.state.utilities || ''} onChange={this.handleChange}/>
        Stores: <input name="stores" value={this.state.stores || ''} onChange={this.handleChange}/>
      </label>
      <button type="submit">Save</button>
    </form>
    </ReactCSSTransitionGroup>
    );
  }
}
export default Bookmark