import React from 'react'
import { NavLink } from 'react-router-dom'
import Icon from './Icons'

class Footer extends React.Component {
render() {
    return (
    <footer className="App-footer">
        <NavLink exact={true} to='/' activeClassName='is-active' className="footer-item"><Icon name="people" color="#5f6067" size={35} /></NavLink>
        <NavLink to='/statistic' activeClassName='is-active' className="footer-item"><Icon name="statistic" color="#5f6067" size={35} /></NavLink>
        <NavLink to='/bookmark' activeClassName='is-active' className="footer-item"><Icon name="bookmark" color="#5f6067" size={35} /></NavLink>
        <NavLink to='/notification' activeClassName='is-active' className="footer-item"><Icon name="notification" color="#5f6067" size={35} /></NavLink>
        <NavLink to='/important' activeClassName='is-active' className="footer-item"><Icon name="important" color="#5f6067" size={35} /></NavLink>
    </footer>
    );
}
}
export default Footer