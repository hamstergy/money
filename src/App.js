import React from 'react';
import './App.scss';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

function App() {
  return (
    <ReactCSSTransitionGroup
    transitionName="fade"
    transitionAppear={true}
    transitionAppearTimeout={1000}>
    <div className="App">
      <div className="App-container">
        <p>
        Who are you and  why you’re  standing here?
        </p>
        <p>
        It doesn't matter if you're building the next Facebook, or if you're the director of marketing for an industrial company.
        </p>
        
        
      </div>
    </div>
    </ReactCSSTransitionGroup>
  );
}
export default App;
