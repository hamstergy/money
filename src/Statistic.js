import React from 'react'
import './Statistic.scss';
import DonutChart from 'react-donut-chart';
import Icon from './Icons'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

class Statistic extends React.Component {
  state = {
    food: 0,
    stores: 0,
    utilities: 0,
    saved: 0
  };

  componentDidMount() {
    const food = parseInt(localStorage.getItem('food') ? localStorage.getItem('food') : '100');
    const stores = parseInt(localStorage.getItem('stores') ? localStorage.getItem('stores') : '300');
    const utilities = parseInt(localStorage.getItem('utilities') ? localStorage.getItem('utilities') : '400');
    const salary = parseInt(localStorage.getItem('salary')? localStorage.getItem('salary') : '1000' );
    const saved = salary-utilities-stores-food
    this.setState({ food,stores,utilities,saved });
  }

  render() {
    const { food, stores, utilities, saved} = this.state;
    return (
      <ReactCSSTransitionGroup
      transitionName="fade"
      transitionAppear={true}
      transitionAppearTimeout={1000}>
      <div className="App-container">
      <div className="title"><span>Statistic</span><div className="title-image"><Icon name="statistic" color="#fff" size={25} /></div></div>
      <div className="month-nav">

        <div className="month-link">January</div>
        <div className="month-link month-link_selected">February</div>
        <div className="month-link">March</div>
        <div className="month-link">April</div>
        <div className="month-link">May</div>
        <div className="month-link">June</div>
        <div className="month-link">July</div>
        <div className="month-link">August</div>
        <div className="month-link">September</div>
        <div className="month-link">October</div>
        <div className="month-link">November</div>
        <div className="month-link">December</div>
        
      </div>
      <svg style={{width:0,height:0}} aria-hidden="true" focusable="false">
        <linearGradient id="first-gradient" x2="1" y2="1">
          <stop offset="0%" stopColor="#FD3F2F" />
          <stop offset="100%" stopColor="#FACE15" />
        </linearGradient>
      </svg>
      <svg style={{width:0,height:0}} aria-hidden="true" focusable="false">
        <linearGradient id="second-gradient" x2="1" y2="1">
          <stop offset="0%" stopColor="#8D4DE8" />
          <stop offset="100%" stopColor="#FF2366" />
        </linearGradient>
      </svg>
      <svg style={{width:0,height:0}} aria-hidden="true" focusable="false">
        <linearGradient id="third-gradient" x2="1" y2="1">
          <stop offset="0%" stopColor="#6956EC" />
          <stop offset="100%" stopColor="#56B2BA" />
        </linearGradient>
      </svg>
      <ReactCSSTransitionGroup
      transitionName="chart"
      transitionAppear={true}
      transitionAppearTimeout={2000}>
        <DonutChart
        width={250}
        height={380}
        legend={false}
        clickToggle= {false}
        strokeColor={'#282c34'}
        innerRadius={0.5}
        selectedOffset ={0.07}
        toggledOffset ={0.04}
        emptyColor={'#000'}
        startAngle={30}
        formatValues={
          (values) => `$${(values)}`
        }
        data={[
        {
          label: 'Foods',
          value: food,
          className: 'food'
        },
        {
          label: 'Saved',
          value: saved,
          isEmpty: true
        },
        {
          label: 'Stores',
          value: stores,
          className: 'stores'
        },
        {
          label: 'Utilities',
          value: utilities,
          className: 'utilities',
        }


      ]}
        />
      </ReactCSSTransitionGroup>
      </div>
      </ReactCSSTransitionGroup>
    );
  }
}

export default Statistic